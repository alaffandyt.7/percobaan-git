import java.util.*;

public class Percobaangit{
    public static void main(String[] args) {
        Scanner inputUser = new Scanner(System.in);
        int menu;
        char ulangMenu;
        
        do {
            System.out.println("======== Tugas 1 =========");
            System.out.println();
            System.out.println("Pilihan Menu");
            System.out.println("------------");
            System.out.println("1. Identitas");
            System.out.println("2. Kalkulator");
            System.out.println("3. Perbandingan");
            System.out.println();
    
            System.out.print("Masukan pilihan menu: ");
            menu = inputUser.nextInt();
            System.out.println();
            switch(menu){
                case 1:
                    System.out.println("Identitas");
                    System.out.println("---------");
                    System.out.println("Nama\t\t\t: Taopik Romdoni");
                    System.out.println("Alasan memilih back-end\t: Taopik Romdoni");
                    System.out.println("Ekspektasi\t\t: Taopik Romdoni");
                    break;
                case 2:
                    int pilihOperator, angka1, angka2;
                    do {
                    System.out.println("Kalkulator");
                    System.out.println("----------");
                    System.out.println("1. Penjumlahan");
                    System.out.println("2. Pengurangan");
                    System.out.println("3. Perkalian");
                    System.out.println("4. Pembagian");
                    System.out.println();
                    System.out.print("Masukan operator : ");
                    pilihOperator = inputUser.nextInt();

                    switch (pilihOperator) {
                        case 1:
                            System.out.println("=== Penjumlahan ===");
                            System.out.print("Masukan angka1 : ");
                            angka1 = inputUser.nextInt();
                            System.out.print("Masukan angka2 : ");
                            angka2 = inputUser.nextInt();
                            int penjumlahan = angka1 + angka2;
                            System.out.println("Hasil penjumlahan : " + angka1 + " + " + angka2 + " = " + penjumlahan);
                            break;
                        case 2:
                            System.out.println("=== Pengurangan ===");
                            System.out.print("Masukan angka1 : ");
                            angka1 = inputUser.nextInt();
                            System.out.print("Masukan angka2 : ");
                            angka2 = inputUser.nextInt();
                            int pengurangan = angka1 - angka2;
                            System.out.println("Hasil penjumlahan : " + angka1 + " - " + angka2 + " = " + pengurangan);
                            break;
                        case 3:
                            System.out.println("=== Perkalian ===");
                            System.out.print("Masukan angka1 : ");
                            angka1 = inputUser.nextInt();
                            System.out.print("Masukan angka2 : ");
                            angka2 = inputUser.nextInt();
                            int perkalian = angka1 * angka2;
                            System.out.println("Hasil kali : " + angka1 + " x " + angka2 + " = " + perkalian);
                            break;
                        case 4:
                            System.out.println("=== Pembagian ===");
                            System.out.print("Masukan angka1 : ");
                            angka1 = inputUser.nextInt();
                            System.out.print("Masukan angka2 : ");
                            angka2 = inputUser.nextInt();
                            float pembagian = angka1 / angka2;
                            System.out.println("Hasil bagi : " + angka1 + " / " + angka2 + " = " + pembagian);
                            break;
                        default :
                            System.out.println("Operator yang anda pilih belum tersedia.");

                    }
                    System.out.print("Anda ingin melakukan perhitungan lagi ? [y/t]");
                    ulangMenu = inputUser.next().charAt(0);
                    }
                    while(ulangMenu == 'y');
                    
                    break;
                case 3:
                    System.out.println("Perbandingan");
                    System.out.println("------------");
                    System.out.println("Masukan angka 1");
                    angka1 = inputUser.nextInt();
                    System.out.println("Masukan angka 2");
                    angka2 = inputUser.nextInt();
                    if (angka1 > angka2) {
                        System.out.println("angka 1 lebih besar dari angka 2");
                    } else {
                        System.out.println("angka 2 lebih besar dari angka 1");
                    }
                    break;
                default :
                    System.out.println("Menu yang anda pilih belum tersedia");
                    break;

            }
            System.out.println("Apakah anda ingin memilih menu lain ? [y/t]");
            ulangMenu = inputUser.next().charAt(0);

        }
        while( ulangMenu == 'y');
    
    }
}